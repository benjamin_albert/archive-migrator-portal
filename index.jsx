require("./node_modules/bootstrap/dist/css/bootstrap.min.css");

import React from 'react';
import ReactDOM from 'react-dom';
import PortalApp from './components/PortalApp';
import PortalActions from './actions/PortalActions';

PortalActions.listen();

ReactDOM.render(<PortalApp />, document.getElementById("myApp"));
