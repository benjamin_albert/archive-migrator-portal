require('./JobListItem.css');
import React from 'react';
import JobListItem from './JobListItem';

export default class JobList extends React.Component {
  render() {
    return (
      <div className="row">
        {this.props.jobs.map(job =>
          <JobListItem key={job.token} job={job} />
        )}
      </div>
    );
  }
}
