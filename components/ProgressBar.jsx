import React from 'react';
import  classNames from 'classnames';

export default class JobListItem extends React.Component {
  render() {
    var props = this.props;
    var percent = Math.floor(props.current / props.total * 100);

    var classes = classNames({
      'progress-bar': true,
      'progress-bar-striped': true,
      'progress-bar-info': percent < 100,
      'progress-bar-success': percent === 100
    });

    return (
      <div className="progress">
        <div
          className={classes}
          role="progressbar"
          style={{width: `${percent}%`}}>
          <span>{percent}%</span>
        </div>
      </div>
    );
  }
}
