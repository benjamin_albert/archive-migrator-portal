require('./PortalApp.css');
import React from 'react';
import JobList from './JobList';
import ProgressStore from '../stores/ProgressStore';

function getStateFromStore() {
  return {
    jobs: ProgressStore.getJobList(),
    jobsPerSecond: ProgressStore.getJobsPerSecond(),
    average: ProgressStore.getAverage()
  };
}

export default class PortalApp extends React.Component {
  constructor() {
    super();
    this.state = getStateFromStore();
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    ProgressStore.on('change', this._onChange);
  }

  componentWillUnmount() {
    ProgressStore.removeListener('change', this._onChange);
  }

  _onChange() {
    this.setState(getStateFromStore());
  }

  render() {
    return (
      <div className="container portal-app">
        <div className="row title">
          Documents per second:
          <span className='stat'>
            {' ' + this.state.jobsPerSecond}
          </span>.
          &nbsp;&nbsp; Average:
          <span className='stat'>
            {' ' + this.state.average}
          </span>.
        </div>
        <JobList jobs={this.state.jobs} />
      </div>
    );
  }
}
