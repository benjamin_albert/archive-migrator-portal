import React from 'react';
import ProgressBar from './ProgressBar';

export default class JobListItem extends React.Component {
  constructor() {
    super();
  }

  render() {
    var job = this.props.job;
    var mifal = job.mifal;


    return (
      <div className="col-xs-12 job-list-item">
        <div className="job-info">
          <span className="job-name">
            {mifal.id} {mifal.name} {job.date}
          </span>

          <span className="doc-count">
            {job.current} of {job.total}
          </span>
        </div>

        <ProgressBar current={job.current} total={job.total} />
      </div>
    );
  }
}
