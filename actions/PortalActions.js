import PortalDispatcher from '../dispatcher/PortalDispatcher';
import io from 'socket.io-client';

export default {
  listen() {
    var addr;
    if (process.env.NODE_ENV !== 'production') {
      addr = `${location.protocol}//${location.hostname}:5000/`;
    }

    var socket = io(addr);

    socket.on('worker:progress', this.updateProgress.bind(this));
  },

  updateProgress(job) {
    PortalDispatcher.dispatch({
      type: 'worker:progress',
      job: job
    });
  }
};
