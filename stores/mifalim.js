export default {
  "2": {
    "id": 2,
    "name": "bneybrak"
  },
  "9": {
    "id": 9,
    "name": "HolonIrya"
  },
  "10": {
    "id": 10,
    "name": "HolonIrya"
  },
  "11": {
    "id": 11,
    "name": "HolonIrya"
  },
  "12": {
    "id": 12,
    "name": "HolonIrya"
  },
  "13": {
    "id": 13,
    "name": "Holon"
  },
  "26": {
    "id": 26,
    "name": "Rehovot"
  },
  "28": {
    "id": 28,
    "name": "RehovotEdu"
  },
  "31": {
    "id": 31,
    "name": "RamatGanSachar"
  },
  "32": {
    "id": 32,
    "name": "RamatGan"
  },
  "34": {
    "id": 34,
    "name": "RamatGMaim"
  },
  "51": {
    "id": 51,
    "name": "Evenyehuda"
  },
  "53": {
    "id": 53,
    "name": "Azur"
  },
  "55": {
    "id": 55,
    "name": "ariel"
  },
  "56": {
    "id": 56,
    "name": "MM_Beer_Yaakov"
  },
  "58": {
    "id": 58,
    "name": "Beit-Shemesh"
  },
  "62": {
    "id": 62,
    "name": "Jaljuliya"
  },
  "64": {
    "id": 64,
    "name": "Ganey-Tikva"
  },
  "67": {
    "id": 67,
    "name": "Taibe"
  },
  "68": {
    "id": 68,
    "name": "Tira"
  },
  "69": {
    "id": 69,
    "name": "yavne"
  },
  "70": {
    "id": 70,
    "name": "Yehud"
  },
  "73": {
    "id": 73,
    "name": "kfarkasem"
  },
  "74": {
    "id": 74,
    "name": "mzion"
  },
  "79": {
    "id": 79,
    "name": "Ness-Ziona"
  },
  "80": {
    "id": 80,
    "name": "Savion"
  },
  "81": {
    "id": 81,
    "name": "Pardes_Hana"
  },
  "82": {
    "id": 82,
    "name": "Pardesia"
  },
  "83": {
    "id": 83,
    "name": "Kadima-Zoran"
  },
  "84": {
    "id": 84,
    "name": "Kalanswa"
  },
  "85": {
    "id": 85,
    "name": "KAZERIN"
  },
  "86": {
    "id": 86,
    "name": "kiryatono"
  },
  "87": {
    "id": 87,
    "name": "KiriatMalahi"
  },
  "88": {
    "id": 88,
    "name": "kiryatyAkron"
  },
  "91": {
    "id": 91,
    "name": "RamatHasharon"
  },
  "92": {
    "id": 92,
    "name": "RamatHasharon"
  },
  "96": {
    "id": 96,
    "name": "Tel-Mond"
  },
  "97": {
    "id": 97,
    "name": "Zemer"
  },
  "100": {
    "id": 100,
    "name": "Emanuel"
  },
  "102": {
    "id": 102,
    "name": "Macbim-Reut"
  },
  "106": {
    "id": 106,
    "name": "KokhavYair"
  },
  "107": {
    "id": 107,
    "name": "MaaleEfrayim"
  },
  "111": {
    "id": 111,
    "name": "HinuhZiona"
  },
  "114": {
    "id": 114,
    "name": "IgudMaimDan"
  },
  "120": {
    "id": 120,
    "name": "KereneMaEfraim"
  },
  "121": {
    "id": 121,
    "name": "EmCalcalit"
  },
  "122": {
    "id": 122,
    "name": "JerusAguda"
  },
  "123": {
    "id": 123,
    "name": "BTMoshavim"
  },
  "159": {
    "id": 159,
    "name": "shafuramuta"
  },
  "166": {
    "id": 166,
    "name": "karneshomca"
  },
  "167": {
    "id": 167,
    "name": "MateBinPS"
  },
  "182": {
    "id": 182,
    "name": "igudareilt"
  },
  "184": {
    "id": 184,
    "name": "KibuyBS"
  },
  "185": {
    "id": 185,
    "name": "KibuyBneBrak"
  },
  "186": {
    "id": 186,
    "name": "KibuyHolon"
  },
  "187": {
    "id": 187,
    "name": "KibuyNAT"
  },
  "188": {
    "id": 188,
    "name": "KibuyPT"
  },
  "189": {
    "id": 189,
    "name": "KibuyRishon"
  },
  "190": {
    "id": 190,
    "name": "Igud vetrinari"
  },
  "191": {
    "id": 191,
    "name": "KibuyRehovot"
  },
  "193": {
    "id": 193,
    "name": "KibuyHerzlia"
  },
  "198": {
    "id": 198,
    "name": "Igudgushdan"
  },
  "210": {
    "id": 210,
    "name": "yahudEDU"
  },
  "235": {
    "id": 235,
    "name": "yavne"
  },
  "256": {
    "id": 256,
    "name": "AmutaBT"
  },
  "266": {
    "id": 266,
    "name": "ZvlonHinch"
  },
  "286": {
    "id": 286,
    "name": "Kalanswa"
  },
  "293": {
    "id": 293,
    "name": "HevelModiinEDU"
  },
  "319": {
    "id": 319,
    "name": "shafuramuta"
  },
  "353": {
    "id": 353,
    "name": "VMHasharon"
  },
  "382": {
    "id": 382,
    "name": "SportBT"
  },
  "395": {
    "id": 395,
    "name": "Givatayim"
  },
  "401": {
    "id": 401,
    "name": "Haifa"
  },
  "403": {
    "id": 403,
    "name": "Haifa"
  },
  "404": {
    "id": 404,
    "name": "Haifa"
  },
  "407": {
    "id": 407,
    "name": "Haifa"
  },
  "408": {
    "id": 408,
    "name": "Haifa"
  },
  "414": {
    "id": 414,
    "name": "tverya"
  },
  "415": {
    "id": 415,
    "name": "Nahariya"
  },
  "416": {
    "id": 416,
    "name": "Nazareth"
  },
  "417": {
    "id": 417,
    "name": "nazareth"
  },
  "419": {
    "id": 419,
    "name": "ako"
  },
  "420": {
    "id": 420,
    "name": "ako"
  },
  "423": {
    "id": 423,
    "name": "Zfat"
  },
  "424": {
    "id": 424,
    "name": "qiryatata"
  },
  "425": {
    "id": 425,
    "name": "kiryatybiyalik"
  },
  "426": {
    "id": 426,
    "name": "kiryatybiyalik"
  },
  "427": {
    "id": 427,
    "name": "kiryat-Yam"
  },
  "428": {
    "id": 428,
    "name": "kiryat-Yam"
  },
  "429": {
    "id": 429,
    "name": "kiryatymotzkin"
  },
  "430": {
    "id": 430,
    "name": "kiryatymotzkin"
  },
  "431": {
    "id": 431,
    "name": "Kiriat-Shmone"
  },
  "432": {
    "id": 432,
    "name": "Kiriat-Shmone"
  },
  "436": {
    "id": 436,
    "name": "Nahariya"
  },
  "440": {
    "id": 440,
    "name": "kiryat-Yam"
  },
  "442": {
    "id": 442,
    "name": "Bueyne"
  },
  "451": {
    "id": 451,
    "name": "Abu-Snan"
  },
  "452": {
    "id": 452,
    "name": "um_el_fahem"
  },
  "453": {
    "id": 453,
    "name": "orakiva"
  },
  "454": {
    "id": 454,
    "name": "Achsal"
  },
  "458": {
    "id": 458,
    "name": "BetShean"
  },
  "459": {
    "id": 459,
    "name": "Binyamina"
  },
  "466": {
    "id": 466,
    "name": "Gush-Halav"
  },
  "467": {
    "id": 467,
    "name": "gisaar"
  },
  "469": {
    "id": 469,
    "name": "Daburia"
  },
  "472": {
    "id": 472,
    "name": "DalyatElCarmel"
  },
  "473": {
    "id": 473,
    "name": "zchronyaakov"
  },
  "474": {
    "id": 474,
    "name": "Horfish"
  },
  "477": {
    "id": 477,
    "name": "Turan"
  },
  "478": {
    "id": 478,
    "name": "tirat"
  },
  "482": {
    "id": 482,
    "name": "Yafia"
  },
  "483": {
    "id": 483,
    "name": "yoqneam"
  },
  "484": {
    "id": 484,
    "name": "Yarka"
  },
  "485": {
    "id": 485,
    "name": "Kabol"
  },
  "486": {
    "id": 486,
    "name": "KfarYassif"
  },
  "487": {
    "id": 487,
    "name": "kfarkana"
  },
  "488": {
    "id": 488,
    "name": "Kfar-Manda"
  },
  "490": {
    "id": 490,
    "name": "Kfar-Kara"
  },
  "491": {
    "id": 491,
    "name": "Kfar-Tavor"
  },
  "492": {
    "id": 492,
    "name": "Carmiel"
  },
  "493": {
    "id": 493,
    "name": "Majar"
  },
  "496": {
    "id": 496,
    "name": "MigdalHamek"
  },
  "500": {
    "id": 500,
    "name": "maalot"
  },
  "502": {
    "id": 502,
    "name": "Nahaf"
  },
  "507": {
    "id": 507,
    "name": "Elabun"
  },
  "508": {
    "id": 508,
    "name": "EinMahl"
  },
  "510": {
    "id": 510,
    "name": "Arara"
  },
  "514": {
    "id": 514,
    "name": "Pkiein"
  },
  "515": {
    "id": 515,
    "name": "kiryatytivon"
  },
  "517": {
    "id": 517,
    "name": "roshpina"
  },
  "518": {
    "id": 518,
    "name": "Reina"
  },
  "522": {
    "id": 522,
    "name": "Shlomi"
  },
  "524": {
    "id": 524,
    "name": "Tamra"
  },
  "525": {
    "id": 525,
    "name": "Bukata"
  },
  "526": {
    "id": 526,
    "name": "Ajar"
  },
  "527": {
    "id": 527,
    "name": "Masade"
  },
  "528": {
    "id": 528,
    "name": "Majdel Shams"
  },
  "530": {
    "id": 530,
    "name": "Shibli"
  },
  "536": {
    "id": 536,
    "name": "KasraSmia"
  },
  "537": {
    "id": 537,
    "name": "Bustan-Elmarj"
  },
  "548": {
    "id": 548,
    "name": "Kaukhab"
  },
  "554": {
    "id": 554,
    "name": "MidGE"
  },
  "559": {
    "id": 559,
    "name": "kiryat-Yam"
  },
  "561": {
    "id": 561,
    "name": "KabautGalilElion"
  },
  "562": {
    "id": 562,
    "name": "IgudArimKabautHadera"
  },
  "563": {
    "id": 563,
    "name": "KibuiEshHaifa"
  },
  "564": {
    "id": 564,
    "name": "KabautAko"
  },
  "566": {
    "id": 566,
    "name": "KibuyZemah"
  },
  "570": {
    "id": 570,
    "name": "KfarVradimAg"
  },
  "572": {
    "id": 572,
    "name": "KfarVradimAm"
  },
  "575": {
    "id": 575,
    "name": "El_Batuf"
  },
  "584": {
    "id": 584,
    "name": "YanuahJat"
  },
  "594": {
    "id": 594,
    "name": "kiryat-Yam"
  },
  "596": {
    "id": 596,
    "name": "AmutatMetav"
  },
  "598": {
    "id": 598,
    "name": "BirElmahsur"
  },
  "601": {
    "id": 601,
    "name": "myosefmichlala"
  },
  "603": {
    "id": 603,
    "name": "amutatisrael"
  },
  "608": {
    "id": 608,
    "name": "DorotAmutatHaver"
  },
  "609": {
    "id": 609,
    "name": "kiryat-Yam"
  },
  "614": {
    "id": 614,
    "name": "HinuhZfatIyuni"
  },
  "615": {
    "id": 615,
    "name": "HinuhZfatDati"
  },
  "618": {
    "id": 618,
    "name": "HaderaTichon"
  },
  "619": {
    "id": 619,
    "name": "kfarvradim"
  },
  "627": {
    "id": 627,
    "name": "HinuhGE"
  },
  "629": {
    "id": 629,
    "name": "RashutTestB"
  },
  "640": {
    "id": 640,
    "name": "Zfat"
  },
  "642": {
    "id": 642,
    "name": "kiryat-Yam"
  },
  "648": {
    "id": 648,
    "name": "Abu-Snan"
  },
  "652": {
    "id": 652,
    "name": "KolheGE"
  },
  "654": {
    "id": 654,
    "name": "Kiriat-Shmone"
  },
  "670": {
    "id": 670,
    "name": "BneZionMehkr"
  },
  "675": {
    "id": 675,
    "name": "BneZionAgud"
  },
  "686": {
    "id": 686,
    "name": "AmutaGE"
  },
  "688": {
    "id": 688,
    "name": "VaadaGE"
  },
  "694": {
    "id": 694,
    "name": "CalcalitGE"
  },
  "701": {
    "id": 701,
    "name": "Eilat"
  },
  "703": {
    "id": 703,
    "name": "Beer-Sheva"
  },
  "713": {
    "id": 713,
    "name": "Mizpe-Ramon"
  },
  "714": {
    "id": 714,
    "name": "Netivot"
  },
  "717": {
    "id": 717,
    "name": "Rahat"
  },
  "732": {
    "id": 732,
    "name": "kerenpash"
  },
  "739": {
    "id": 739,
    "name": "Beer-ShevaKibuy-Esh"
  },
  "742": {
    "id": 742,
    "name": "IgudAshkelon"
  },
  "758": {
    "id": 758,
    "name": "Tipuhhinuh"
  },
  "773": {
    "id": 773,
    "name": "Harhevron"
  },
  "774": {
    "id": 774,
    "name": "matnshhevron"
  },
  "797": {
    "id": 797,
    "name": "Arava_tichona"
  },
  "803": {
    "id": 803,
    "name": "segevshalom"
  },
  "805": {
    "id": 805,
    "name": "ksfia"
  },
  "812": {
    "id": 812,
    "name": "VNMarav"
  },
  "813": {
    "id": 813,
    "name": "VNMizrah"
  },
  "816": {
    "id": 816,
    "name": "Beer-Sheva"
  },
  "937": {
    "id": 937,
    "name": "CalcalitMegido"
  },
  "938": {
    "id": 938,
    "name": "AmutaMegido"
  },
  "939": {
    "id": 939,
    "name": "ZvlonPituah"
  },
  "949": {
    "id": 949,
    "name": "BakaElGarbia"
  },
  "957": {
    "id": 957,
    "name": "KibuyRG"
  },
  "958": {
    "id": 958,
    "name": "KibuyGivataim"
  },
  "960": {
    "id": 960,
    "name": "Shfarams"
  },
  "961": {
    "id": 961,
    "name": "JerusalemMD"
  },
  "962": {
    "id": 962,
    "name": "AshkelonMD"
  },
  "963": {
    "id": 963,
    "name": "JerusalemMD"
  },
  "964": {
    "id": 964,
    "name": "JerusalemHm"
  }
};
