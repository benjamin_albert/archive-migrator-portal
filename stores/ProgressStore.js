import {EventEmitter} from 'events';
import PortalDispatcher from '../dispatcher/PortalDispatcher';
import mifalim from './mifalim';

var jobs = {};

var total = 0;
var count = 0;
var newJobs = 0;
var jobsPreSecond = 0;


function x() {
setTimeout(() => {
  newJobs++;
  x();
}, Math.random() * 150);
}x();

setInterval(() => {
  jobsPreSecond = newJobs;
  total += jobsPreSecond;
  count++;
  newJobs = 0;
  store.emitChange();
}, 1000);

class ProgressStore extends EventEmitter {
  getAll() {
    return jobs;
  }

  getJobsPerSecond() {
    return jobsPreSecond;
  }

  getAverage() {
    return count ? Math.ceil(total / count) : 0;
  }

  getJobList() {
    var jobList = [];
    for (var token in jobs) {
      jobList.push(jobs[token]);
    }

    jobList.sort( (a, b) => a.date - b.date );

    return jobList;
  }

  get(token) {
    return jobs[token];
  }

  emitChange() {
    this.emit('change');
  }
}

var store = new ProgressStore();

PortalDispatcher.register(function(action) {
  switch (action.type) {
    case 'worker:progress':
      var job = action.job;

      if (typeof job.mifal === 'number') {
        job.mifal = mifalim[job.mifal];
      }

      jobs[action.job.token] = job;
      newJobs++;
      total++;
      store.emitChange();
      break;
  }
});

export default store;
